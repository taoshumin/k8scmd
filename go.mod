module test.io/cmd

go 1.18

require (
	github.com/spf13/cobra v1.6.1
	github.com/spf13/pflag v1.0.5
)

require github.com/inconshreveable/mousetrap v1.0.1 // indirect
